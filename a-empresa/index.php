<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.js"/> -->
	<title>Elite Imóveis - A Empresa</title>
</head>
<body class="container">
	<?php include_once "../templates/header.php"; ?>

	<!-- Conteúdo -->
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-empresa">
				<div class="panel-heading quem-somos">
					Quem somos ?
				</div>
				<div class="panel-body text-justify">
				Somos especializados em real estate business e negociações imobiliárias, atuando no Brasil e no exterior com compra e venda de empresas e imóveis destinados a tornar-se fonte de renda, investimentos ou para capitalização de recursos financeiro.Nossa missão:hoje em dia, o setor imobiliário não se restringe somente à compra e venda de imóveis somente com intuito de se obter uma moradia, mas também como fonte de investimentos ou para capitalização de recursos financeiros. Pensando nisso, a Elite Business Real Estate tem como missão disponibilizar ao mercado maiores e melhores condições de negociações, proporcionando o crescimento das pessoas efavorecendo melhor sua qualidade de vida para a sociedade.
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-empresa">
				<div class="panel-heading visao">
					Visão
				</div>
				<div class="panel-body text-justify">
				Nossa visão e nossos objetivos: Dentro de pouco tempo ser reconhecida por nossos clientes, parceiros e colaboradores como uma empresa que se destaca pela qualidade e prontidão dos nossos serviços, nos tornando referencia no mercado. A Elite Business Real Estate acredita que: Investir em imóveis é seguro: é um patrimônio, está lá, ninguém lhe tira, esta fora do alcance do governo e intervenções econômicas. Imóvel é fonte de renda, sendo bem escolhido é fonte de renda vitalícia, todos precisam de moradia e quem não tem condições de comprar a casa própria sempre estará a procura de um bom imóvel para alugar. O seu investimento pode ser utilizado como uma fonte de lazer, um lugar para passar ferias, veraneio e etc. Imóveis brasileiros com relaçao aos do mundo encontram-se em estado de valorização constante.Ha uma grande demanda no mercado, ou seja é fácil transformar seu imóvel em capital financeiro.
				</div>
			</div>
		</div>
	</div>
	
	
	<?php include_once "../templates/footer.php"; ?>
</body>
</html>