<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.js"/> -->

	<title>Elite Imóveis</title>
</head>
<body class="container">
	<?php include_once "../templates/header.php"; ?>

	<!-- Box -->
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel1 panel-empresa">
				<div class="panel-heading">
					<span><img src="../img/icon/icone_assessoria.png" class="icon"></span>
					Assessorio Imobiliário
				</div>
				<div class="panel-body text-justify">
					Existem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção.
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel1 panel-empresa">
				<div class="panel-heading">
					<span><img src="../img/icon/icone_vantagens.png" class="icon"></span>
					Vantagens
				</div>
				<div class="panel-body text-justify">
					Existem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção.	
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel1 panel-empresa">
				<div class="panel-heading">
					<span><img src="../img/icon/icone_projeto.png" class="icon"></span>
					Projeto Completo
				</div>
				<div class="panel-body text-justify">
					Existem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção.
				</div>
			</div>
		</div>
	</div>
	<?php include_once "../templates/footer.php"; ?>
</body>
</html>