<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.js"/> -->
	<title>Elite Imóveis - Imóveis</title>
</head>
<body class="container">
	<?php include_once "../templates/header.php"; ?>

	<!-- Conteúdo -->
	<div class="row">
		<div class="col-md-12">
			<center><blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</blockquote></center>
		</div>
	</div>

	<!-- Blocos -->
	<?php if(@$_GET['pag']): ?>
		<?php
			$pag = $_GET['pag'];
			switch ($pag) {
				case 4:
					include_once "../templates/imo01.php";
					break;
				case 5:
					include_once "../templates/imo02.php";
					break;
				
				case 6:
					include_once "../templates/imo03.php";
					break;
				case 7:
					include_once "../templates/imo04.php";
					break;
			}
		?>
	<?php else: ?>
	<div class="row">
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-body texto-sobre">
					<a href="?pag=4">
							<span class="span">
								<span style="font-size: 40px;" class="glyphicon glyphicon-ok"></span>
									<br>Disponível para: 2016/04/10<br>Lorem ipsum dolor sit amet, consectetur.
								</span>
							<center><img src="../img/imovel-1.png" class="imagem-body"/></center>
					</a>
					<!-- <center><img src="../img/imovel-1.png" class="imagem-body"></center> -->

				</div>
				<div class="panel-body body-imoveis">
					<h3>Dunas Village Cumbuco 01</h3>
					<small>Lorem ipsum dolor sit amet.</small>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-body texto-sobre">
					<a href="?pag=5">
							<span class="span">
								<span style="font-size: 40px;" class="glyphicon glyphicon-ok"></span>
									<br>Disponível para: 2016/04/10<br>Lorem ipsum dolor sit amet, consectetur.
								</span>
							<center><img src="../img/imovel-1.png" class="imagem-body"/></center>
					</a>
					<!-- <center><img src="../img/imovel-1.png" class="imagem-body"></center> -->

				</div>
				<div class="panel-body body-imoveis">
					<h3>Dunas Village Cumbuco 02</h3>
					<small>Lorem ipsum dolor sit amet.</small>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-body texto-sobre">
					<a href="?pag=6">
							<span class="span">
								<span style="font-size: 40px;" class="glyphicon glyphicon-ok"></span>
									<br>Disponível para: 2016/04/10<br>Lorem ipsum dolor sit amet, consectetur.
								</span>
							<center><img src="../img/imovel-1.png" class="imagem-body"/></center>
					</a>
					<!-- <center><img src="../img/imovel-1.png" class="imagem-body"></center> -->

				</div>
				<div class="panel-body body-imoveis">
					<h3>Dunas Village Cumbuco 03</h3>
					<small>Lorem ipsum dolor sit amet.</small>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-body texto-sobre">
					<a href="?pag=7">
							<span class="span">
								<span style="font-size: 40px;" class="glyphicon glyphicon-ok"></span>
									<br>Disponível para: 2016/04/10<br>Lorem ipsum dolor sit amet, consectetur.
								</span>
							<center><img src="../img/imovel-1.png" class="imagem-body"/></center>
					</a>
					<!-- <center><img src="../img/imovel-1.png" class="imagem-body"></center> -->

				</div>
				<div class="panel-body body-imoveis">
					<h3>Dunas Village Cumbuco  04</h3>
					<small>Lorem ipsum dolor sit amet.</small>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<?php include_once "../templates/footer.php"; ?>
</body>
</html>