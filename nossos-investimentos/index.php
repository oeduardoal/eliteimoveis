<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.js"/> -->
	<title>Elite Imóveis - Nossos Investimentos</title>
</head>
<body class="container">
	<?php include_once "../templates/header.php"; ?>

	<!-- Conteúdo -->
	<?php if(@$_GET['pag']): ?>

		<?php
			$pag = $_GET['pag'];
			switch ($pag) {
				case 1:
					include_once "../templates/res01.php";
					break;
				case 2:
					include_once "../templates/res02.php";
					break;
				case 3:
					include_once "../templates/res03.php";
					break;
				
				default:
					# code...
					break;
			}
		?>

	<?php else: ?>
	<div class="row">
		<div class="col-md-4 investimentos">
			<div class="panel">
			<a href="?pag=1">
				<div class="panel-heading">
						<h4>Residêncial <i>Odilon</i></h4>
				</div>
				<div class="panel-body">
					<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>

				</div>
			</a>
			<div class="panel-body">
					<img src="../img/residencial-1.png">
				</div>
			</div>

		</div>
		<div class="col-md-4 investimentos">
			<div class="panel">
			<a href="?pag=2">
				<div class="panel-heading">
						<h4>Residêncial <i>Plazza</i></h4>
				</div>
				<div class="panel-body">
					<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
				</div>
			</a>
			<div class="panel-body">
					<img src="../img/residencial-2.png">
				</div>
			</div>

		</div>
		<div class="col-md-4 investimentos">
			<div class="panel">
			<a href="?pag=3">
				<div class="panel-heading">
						<h4>Residêncial <i>Center </i></h4>
				</div>
				<div class="panel-body">
					<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
				</div>
			</a>
			<div class="panel-body">
					<img src="../img/residencial-3.png">
				</div>
			</div>

		</div>

	</div>
	<?php endif; ?>
	<?php include_once "../templates/footer.php"; ?>
</body>
</html>