	
	<!-- Bibliotecas -->
	<script src="../js/jquery-latest.min.js"></script>
	<script src="../js/jquery.slides.js"></script>
	<script src="../js/main.js"></script>

<div class="page-header">
	<ul class="lista">
		<li class="item"><a href="../home/">Home</a></li>
		<li class="item"><a href="../a-empresa/">A Empresa</a></li>
		<li class="item"><a href="../nossos-servicos/">Nossos Serviços</a></li>
		<li class="item"><a href="../nossos-investimentos">Nossos Investimentos</a></li>
		<li class="item"><a href="../imoveis">Imóveis</a></li>
		<li class="item"><a href="../contato">Contato</a></li>
	</ul>
</div>


<div class="row">
	
	<!-- img1 -->
	<div class="col-md-4">
		<img class="logo" src="../img/logo.png" alt="Elite Fortaleza">
	</div>

	<!-- Slide -->
	<div class="col-md-8 slides">
		<div id="slides">
			<img src="../img/baner.jpg">
			<img src="../img/baner1.jpg">
			<img src="../img/baner2.jpg">
			<img src="../img/baner3.jpg">
		</div>
	</div>
	
</div>
