
<div class="row">
	<div class="col-md-6">
		<div class="panel">
				<div class="panel-body">
							<center><img src="../img/imovel-1.png" class=""/></center>
				</div>
				<div class="panel-body body-imoveis">
					<h3>Dunas Village Cumbuco</h3>
					<small>Lorem ipsum dolor sit amet.</small>
				</div>
			</div>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3731.7502101405826!2d-46.614014885405254!3d-20.720365568721185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94b6c3ca133b0943%3A0x504e307d533ff748!2sElite+Im%C3%B3veis!5e0!3m2!1spt-BR!2sbr!4v1460299932473" frameborder="0" width="100%" height="240" style="border:0" allowfullscreen></iframe>
	</div>

	<div class="col-md-6 color-white">
		<blockquote>Dunas Village Cumbuco 01</blockquote>
		<p>Disponível a partir de: 2012/10/01
		<br>
		Preço: R $ 444.000
		<br>
		Encargos: Consulte
		<br>
		Taxas: Consulta
		<br>
		<br>
		<h4>Descrição:</h4>
		Espaço vital: 123 m²
		<br>
		Área Total: 210 m²
		<br>
		Número de quartos: 3
		<br>
		Número de andares: 2
		<br>
		Número de casas de banho: 2
		<br>
		Número de antibagni: 3
		<br>
		<br>
		<p class="text-justify">O Dunas Village é a harmonia perfeita entre arquitetura e natureza tropical. As 18 moradias independentes com piscina, sala de jantar e área de lazer estão localizados apenas a 180 mt. a esplêndida praia e 50 metros das dunas brancas que cercam a localidade. Projetado para aqueles que gostam de desportos náuticos, natureza, tranquilidade, mas também a beleza e qualidade de infra-estrutura, materiais e soluções técnicas utilizados na construção.</p></p>
	</div>

</div>